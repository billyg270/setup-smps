echo "=== BY RUNNING THIS SCRIPT YOU AGREE TO THE MINECRAFT SERVER EULA ==="
echo "Cancel the script now if not"

# Get inputs
echo Folder Name:
read folderName

echo MC Version:
read mcVersion

echo MOTD:
read mcMotd

echo RAM to assign in GB:
read mcRam

# Install dependencies
apt-get update
apt install -y openjdk-17-jdk openjdk-17-jre
apt install -y curl
apt install -y jq
apt install -y screen

# Setup MC server
mkdir $folderName
cd $folderName

# Get the build number of the most recent build
latest_build="$(curl -sX GET https://papermc.io/api/v2/projects/paper/versions/${mcVersion}/builds -H 'accept: application/json' | jq '.builds [-1].build')"
# Construct download URL
paper_jar_name="paper-${mcVersion}-${latest_build}.jar"
download_url="https://papermc.io/api/v2/projects/paper/versions/${mcVersion}/builds/${latest_build}/downloads/${paper_jar_name}"
# Download paper
wget "${download_url}"

# Setup configs
echo "eula=true" >> eula.txt

echo "enable-command-block=true" >> server.properties
echo "motd=${mcMotd}" >> server.properties
echo "allow-flight=true" >> server.properties
echo "view-distance=7" >> server.properties
echo "sync-chunk-writes=false" >> server.properties
echo "simulation-distance=6" >> server.properties
echo "spawn-protection=0" >> server.properties

# Setup run.sh
echo "java -Xms${mcRam}G -Xmx${mcRam}G -jar ${paper_jar_name} --nogui" >> run.sh
chmod 777 run.sh

# Start server
screen -d -m ./run.sh

echo "MC server is starting, type 'screen -r' to view console."
rm paper.sh