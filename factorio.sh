# Inputs
echo "Existing save file to use:"
echo "Don't include .zip"
echo "Leave blank to generate new world"
read saveName

# Install dependencies
apt-get update
apt install -y screen

# Download server
cd /opt/
wget -O f.tar.xz https://factorio.com/get-download/stable/headless/linux64
tar -xJf f.tar.xz
rm f.tar.xz
cd /opt/factorio/
wget https://raw.githubusercontent.com/Entroper/factorio-server/master/settings/map-gen/rail-world.json
cd /root/

myIp=$(curl ifconfig.me)
if [ $saveName -eq "" ]
then
	echo === Add any mods or other files now by going to sftp://root@${myIp}/opt/factorio/ ===
else
	echo === Add the save file, any mods and other files now by going to sftp://root@${myIp}/opt/factorio/ ===
fi
echo === Then press enter to continue ===
read x

# Setup
useradd factorio
mkdir /home/factorio
chown -R factorio:factorio /opt/factorio

if [ $saveName -eq "" ]
then
	echo "Generating new save file"
	runuser -l factorio -c 'cd /opt/factorio && ./bin/x64/factorio --create ./saves/save.zip --map-gen-settings rail-world.json'
else
	echo "Will not generate new save file"
fi

# Run script
if [ $saveName -eq "" ]
then
	echo "runuser -l factorio -c 'cd /opt/factorio && screen -d -m ./bin/x64/factorio --start-server save'" >> run.sh
else
	echo "runuser -l factorio -c 'cd /opt/factorio && screen -d -m ./bin/x64/factorio --start-server ${saveName}'" >> run.sh
fi
chmod 777 run.sh

# Start server
screen -d -m ./run.sh

echo "Factorio server is starting, type 'screen -r' (as factorio user) to view console."
rm factorio.sh
